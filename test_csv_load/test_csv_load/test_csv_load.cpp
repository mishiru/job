﻿#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include<unordered_map>

using String = std::string;
using Strings = std::vector<std::string>;
using Map = std::unordered_map<Strings, int>;
//プロトタイプ宣言
bool exists(std::ifstream&);
void aggReGate(const Strings&,Map&);
Strings split(String&, char);
//
template <>
struct  std::hash<Strings>
{
	std::size_t operator()(const Strings& k) const
	{
		size_t result = 0;
		for (auto it = k.begin(); it != k.end(); it++) {
			auto s = *it;
			auto code = std::hash<String>()(s);
			result = result * 997 + code;
		}
		return result;
	}
};

//グローバル変数
//Map aggregateMap;

int main()
{
	Map aggregateMap;
	std::ifstream ifs("test.csv");
	exists(ifs);
	String line;
	
	int i = 0;
	while (std::getline(ifs, line)) {
		Strings lines;
		lines.push_back(line);
		aggReGate(lines,aggregateMap);
	}
	for (auto itr = aggregateMap.begin(); itr != aggregateMap.end(); ++itr) {
		for (auto it = itr->first.begin(); it != itr->first.end(); it++) {
			auto s = *it;
			std::cout << s << ",";        // キーを表示
		}
		std::cout << itr->second << "\n";    // 値を表示
	}
}
void aggReGate(const Strings& cstrs,Map& map)
{
	auto itr = map.find(cstrs);
	if (itr != map.end())
	{
		map[cstrs]++;
	}
	else
	{
		map[cstrs] = 1;
	}
}

Strings split(String& input, char delimiter)
{
	std::istringstream stream(input);
	String field;
	Strings result;
	while (getline(stream, field, delimiter)) {
		result.push_back(field);
	}
	return result;
}

bool exists(std::ifstream& ifs) {
	if (!ifs) {
		printf("No find file\n");
		return false;
	}
	else {
		printf("file open\n");
		return true;
	}
}